package com.company;

import com.company.model.Book;
import com.company.model.Journal;
import com.company.model.Yearbook;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Book[] books = new Book[3];
        books[0] = new Book("Figt Club", "Chuck Palahniuk", "W.W. Norton & Company", 1996);
        books[1] = new Book("Ninteen Eighty-For", "George Orwell", "Secker and Warbug", 1949);
        books[2] = new Book("Lolita", "Vladimir Nabokov", "Olimpia Press", 1955);
        Journal[] journals = new Journal[3];
        journals[0] = new Journal("Murzilka", "Children's literary and art magazine", 1924, "May");
        journals[1] = new Journal("The young naturalist", "popular science magazine", 1954, "October");
        journals[2] = new Journal("Young technician", "scientific magazine", 1995, "April");
        Yearbook[] yearbooks = new Yearbook[3];
        yearbooks[0] = new Yearbook("The World Factbook", " almanac-style information", 1954);
        yearbooks[1] = new Yearbook("Science and humanity", "scientific", 1962);
        yearbooks[2] = new Yearbook("French Yearbook", "historical", 2000);
        Scanner in = new Scanner(System.in);
        System.out.println("Enter year publishing: ");
        int year = Integer.parseInt(in.nextLine());
        for (int i = 0; i < 3; i++) {
            if (year == books[i].getYearPublishing()) {
                System.out.println("Название: " + books[i].getName() + " Автор: " + books[i].getAuthor() + " Издательство: " + books[i].getPublishingHouse() + "  Год издания " + books[i].getYearPublishing());

            }
            if (year == journals[i].getYearPublishing()) {
                System.out.println("Название: " + journals[i].getName() + " Тематика: " + journals[i].getSubjects() + "  Год издания " + journals[i].getYearPublishing() + " Месяц: " + journals[i].getMonth());


            }
            if (year == yearbooks[i].getYearPublishing()) {
                System.out.println("Название: " + yearbooks[i].getName() + " Тематика: " + yearbooks[i].getSubjects() + "  Год издания " + yearbooks[i].getYearPublishing());
            } else {
                System.out.println("Такого журнала нет");
                break;
            }

        }
    }}


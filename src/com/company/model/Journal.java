package com.company.model;

public class Journal {
    private String name;
    private String subjects;
    private int yearPublishing;
    private String month;

    public Journal(String name, String subjects, int yearPublishing, String month) {
        this.name = name;
        this.subjects = subjects;
        this.yearPublishing = yearPublishing;
        this.month = month;
    }

    public String getName() {
        return name;
    }

    public String getSubjects() {
        return subjects;
    }

    public int getYearPublishing() {
        return yearPublishing;
    }

    public String getMonth() {
        return month;
    }
}

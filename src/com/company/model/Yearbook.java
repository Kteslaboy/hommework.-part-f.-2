package com.company.model;

public class Yearbook {
    private String name;
    private String subjects;
    private int yearPublishing;

    public Yearbook(String name, String subjects, int yearPublishing) {
        this.name = name;
        this.subjects = subjects;
        this.yearPublishing = yearPublishing;
    }

    public String getName() {
        return name;
    }

    public String getSubjects() {
        return subjects;
    }

    public int getYearPublishing() {
        return yearPublishing;
    }
}

package com.company.model;

public class Book {
    private String name;
    private String author;
    private String publishingHouse;
    private int yearPublishing;


    public Book(String name, String author, String publishingHouse, int yearPublishing) {
        this.name = name;
        this.author = author;
        this.publishingHouse = publishingHouse;
        this.yearPublishing = yearPublishing;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public int getYearPublishing() {
        return yearPublishing;
    }
}

